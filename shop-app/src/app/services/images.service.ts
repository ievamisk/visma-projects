import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPicture } from '../interfaces/picture';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ImagesService {
    private BASE_API_URL: string = 'http://localhost:3000/';
    private IMAGES_URL: string = this.BASE_API_URL + 'images';

    constructor(private http: HttpClient) { }
 
    getPictures(): Observable<IPicture[]> {
        const url = this.IMAGES_URL;
        return this.http.get<Array<IPicture>>(url);
    }
}
