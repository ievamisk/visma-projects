import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { IShopItem } from '../interfaces/shop-item';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ShopItemsService {
    private BASE_API_URL: string = 'http://localhost:3000/';
    private ITEMS_URL: string = this.BASE_API_URL + 'items/';

    private shopItems: IShopItem[];
    public shopItemsSubject: Subject<IShopItem[]> = new Subject<IShopItem[]>();

    constructor(private http: HttpClient) {}

    getItems(): void {
        const url = this.ITEMS_URL;
        this.http.get<IShopItem[]>(url).subscribe(items => {
            this.shopItems = items;
            this.shopItemsSubject.next(items);
        });
	}

	getItem(id: number): IShopItem {
        return this.shopItems.find(shopItem => shopItem.id === id);
    }

    addItem(shopItem: IShopItem): void {
        this.http.post<IShopItem>(this.ITEMS_URL, shopItem, httpOptions).subscribe(item => {
            this.shopItems = [
                ...this.shopItems,
                item
            ];
            this.shopItemsSubject.next(this.shopItems);
        });
    }

    editItem(shopItem: IShopItem, id: number): void {
        const url = this.ITEMS_URL + id;
        this.http.put<IShopItem>(url, shopItem, httpOptions).subscribe(shopItem => {
            this.shopItems = this.shopItems.map(item => {
                if (item.id === shopItem.id) {
                    return shopItem;
                } else {
                    return item;
                }
            });

            this.shopItemsSubject.next(this.shopItems);
        });
    }

    deleteItem(id: number) {
        const url = this.ITEMS_URL + id;
        return this.http.delete<IShopItem>(url, httpOptions).subscribe(item => {
            this.shopItems = this.shopItems.filter(item => item.id !== id);

            this.shopItemsSubject.next(this.shopItems);
        });
    }
}