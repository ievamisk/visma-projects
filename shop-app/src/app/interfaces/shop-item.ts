export interface IShopItem {
    name: string,
    price: number,
    image: string,
    discount: number,
    id: number
}