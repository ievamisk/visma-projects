import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formatPrice' })
export class FormatPrice implements PipeTransform {
	transform(price: number) {
		let formatedPrice = price.toFixed(2);
		return formatedPrice;
	}
}