import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IShopItem } from '../../../interfaces/shop-item';
import { EditFormComponent } from '../edit-form/edit-form.component';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss']
})

export class ShopItemComponent implements OnInit {
	@Input() shopItem: IShopItem;

	constructor(
        private modalService: NgbModal,
    ) { }

	ngOnInit() {
    }

    openModal() {
        const modalRef = this.modalService.open(EditFormComponent, { centered: true });
        modalRef.componentInstance.shopItem = this.shopItem;
        modalRef.componentInstance.title = 'Edit';
    }

}
