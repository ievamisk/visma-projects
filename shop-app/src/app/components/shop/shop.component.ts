import { Component, OnInit } from '@angular/core';
import { EditFormComponent } from './edit-form/edit-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

    constructor(private modalService: NgbModal) { }

    ngOnInit() {
    }

    openModal() {
        const modalRef = this.modalService.open(EditFormComponent, { centered: true });
        modalRef.componentInstance.title = 'Add';
    }
}
