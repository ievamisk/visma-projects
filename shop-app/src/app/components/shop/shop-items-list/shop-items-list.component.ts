import { Component, OnInit, Input } from '@angular/core';
import { IShopItem } from '../../../interfaces/shop-item';
import { ShopItemsService } from '../../../services/shop-items.service';
import { Subscription } from '../../../../../node_modules/rxjs';

@Component({
	selector: 'app-shop-items-list',
	templateUrl: './shop-items-list.component.html',
	styleUrls: ['./shop-items-list.component.scss']
})
export class ShopItemsListComponent implements OnInit {
    private shopItems: IShopItem[];
	private subscription: Subscription;
	
    constructor(private shopItemsService: ShopItemsService) { 
		this.subscription = new Subscription();
		this.subscription = this.shopItemsService.shopItemsSubject.subscribe(shopItems => {
			this.shopItems = shopItems;
		});
	}

	ngOnInit() {
        this.getItems();
	}

	getItems() {
		this.shopItemsService.getItems()
	}
}