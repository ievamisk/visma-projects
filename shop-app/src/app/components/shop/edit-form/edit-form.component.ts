import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IShopItem } from '../../../interfaces/shop-item';
import { IPicture } from '../../../interfaces/picture';
import { ImagesService } from '../../../services/images.service';
import { ShopItemsService } from '../../../services/shop-items.service';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
    private images: IPicture[];
    private hasDiscount: boolean;
    @Input() shopItem: IShopItem;
    @Input() image: IPicture;

    constructor(
        private imagesService: ImagesService,
        private shopItemsService: ShopItemsService,
        private activeModal: NgbActiveModal
    ) {}

    ngOnInit() {
        this.shopItem = this.shopItem || {} as IShopItem;
        this.hasDiscount = this.shopItem.discount ? true : false;
        this.getPictureOptions();
    }

    addItem() {
        this.shopItemsService.addItem(this.shopItem as IShopItem);

        this.activeModal.close(this.shopItem);
    }

    editItem() {
        let updatedShopItem: IShopItem = this.shopItem;

        if (!this.hasDiscount) {
            updatedShopItem = Object.assign(this.shopItem, { discount: null });
        }

        this.shopItemsService.editItem(updatedShopItem as IShopItem, updatedShopItem.id);
        this.activeModal.close();
    }

    deleteItem() {
        this.shopItemsService.deleteItem(this.shopItem.id);
        this.activeModal.close(this.shopItem);
    }

    getPictureOptions() {
        this.imagesService.getPictures()
            .subscribe(images => this.images = images);
    }

    addDiscount() {
        this.hasDiscount = !this.hasDiscount;
    }
}
