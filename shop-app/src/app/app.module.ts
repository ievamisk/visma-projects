import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { FormatPrice } from './utils/price.pipe';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SortingHeaderComponent } from './components/sorting-header/sorting-header.component';
import { ShopComponent } from './components/shop/shop.component';
import { ShopItemComponent } from './components/shop/shop-item/shop-item.component';
import { ShopItemsListComponent } from './components/shop/shop-items-list/shop-items-list.component';
import { EditFormComponent } from './components/shop/edit-form/edit-form.component';


@NgModule({
    entryComponents: [ EditFormComponent ],
	declarations: [
		AppComponent,
		NavbarComponent,
		ShopItemsListComponent,
		ShopItemComponent,
		SidebarComponent,
		SortingHeaderComponent,
		EditFormComponent,
		FormatPrice,
		ShopComponent
	],
	imports: [
        BrowserModule,
        HttpClientModule,
        NgbModule.forRoot(),
        FormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
