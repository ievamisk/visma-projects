import { ItemsList } from './components/itemsList/itemsList.js';
import { Modal } from './components/modal/modal.js';
import { FormEditCreate } from './components/formEditCreate/formEditCreate.js';

const app = (() => {

	function createItem() {
		const form = new FormEditCreate();
		const modal = new Modal();
		modal.renderContent(form.renderForm());
		form.init();
	}

	function getItems() {
		const items = new ItemsList();
		return items.init();
	}

	return {
		openModal: createItem,
		getItems: getItems
	};
})();

window.addEventListener('load', () => {
	const addButton = document.querySelector('.add-button');

	addButton.addEventListener('click', () => {
		app.openModal();
		// app.getItems();

	});

	// addButton.onclick = app.openModal;
	app.getItems();
});