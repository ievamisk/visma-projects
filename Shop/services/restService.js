import * as constants from '../constants/constants.js';

export class RestService {

	static get(route, id) {
		const url = `${ constants.BASE_API_URL }${ route }/${ id ? id : '' }`;
		return fetch(url)
			.then(response => response.json())
			.catch(error => console.error(error));
	}

	static delete(route, id) {
		const url = `${ constants.BASE_API_URL }${ route }/${ id }`;

		fetch(url, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.catch(error => console.error(error));
	}

	static submit(route, id, data) {
		const url = `${ constants.BASE_API_URL }${ route }/${ id ? id : '' }`;
		const requestMethod = id ? 'PUT' : 'POST';

		return fetch(url, {
			method: requestMethod,
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(response => response.json())
			.catch(error => console.error(error));
	}
}
