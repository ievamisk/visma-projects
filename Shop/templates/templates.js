export class Templates {
    static getOptionItem(option) {
        return `<option value=${option.url}>${option.name}</option>`;
    }
    
    static getFormHeader(title) {
        return `<h2 class="form-title">${title} item</h2>`;
    }
}