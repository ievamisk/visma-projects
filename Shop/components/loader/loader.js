import { loader } from './loaderTemplate.js';

export class Loader {
	static start() {
		this.itemsList = document.querySelector('.card-list');
		this.itemsList.insertAdjacentHTML('afterbegin', loader());
	}
}