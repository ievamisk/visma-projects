import { ItemsList } from '../itemsList/itemsList.js';

export class Modal {
	constructor() {
		this.modal = document.querySelector('.modal');
		this.modal.classList.toggle('visible');
		this.modal = document.querySelector('.modal');
		this.modalContent = this.modal.querySelector('.modal-content');
		this.modal.onclick = this.toggleModal;
	}

	toggleModal(event) {
		const modalTimeout = 400;
		this.modal = document.querySelector('.modal');
		this.deleteButton = document.querySelector('.button-delete');
		this.saveButton = document.querySelector('.button-save');

		if (event.target === this.modal ||
			event.target === this.saveButton ||
			event.target === this.deleteButton
		) {
			this.modal.classList.toggle('visible');
			setTimeout(() => {
				this.modal.children[0].innerHTML = '';
				//should not call items here, but I dunno where else
				new ItemsList().getItems();
			}, modalTimeout);
		}
	}

	renderContent(content) {
		return this.modalContent.insertAdjacentHTML('beforeend', content);
	}
}