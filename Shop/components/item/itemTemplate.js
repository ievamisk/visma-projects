import { formatPrice } from '../../utils/priceUtils.js';

export const listItem = (item) => {
	return `<li class="card-list-item" id=${ item.id }>
                <figure class="card-list-item-figure ${ item.discount ? 'sale' : '' }">
                    <img class="card-list-item-image" alt="t-shirt" src=${ item.image }>
                    <div class="card-list-item-details">
                        <h4 class="card-list-item-title">${ item.name }</h4>
                        <div class="card-list-item-price">
                            <span class="card-list-item-price-sale ${ item.discount ? '' : 'hidden' }">
                                ${ item.discount ? formatPrice(item.price) : '' }
                            </span>
                            <span class="card-list-item-price-regular">
                                ${ item.discount ? formatPrice(item.price-item.discount) : formatPrice(item.price) }
                            </span>
                        </div>
                        <button type="button" class="card-list-item-button">Add to cart</button>
                    </div>
                </figure>
                <button type="button" class="card-list-item-edit">Edit</button>
            </li>`;
};