import { listItem } from './itemTemplate.js';
import { FormEditCreate } from '../formEditCreate/formEditCreate.js';
import { Modal } from '../modal/modal.js';

export class Item {
	constructor(item) {
		this.item = item;
		this.editItem = this.editItem.bind(this);
	}

	renderItem() {
		return listItem(this.item);
	}

	editItem() {
		const modal = new Modal();
		const form = new FormEditCreate(this.item.id);
		modal.renderContent(form.renderForm());
		form.init();
	}
}