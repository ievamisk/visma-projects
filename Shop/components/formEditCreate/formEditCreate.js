import { RestService } from '../../services/restService.js';
import { Templates } from '../../templates/templates.js';
import { form } from './formTemplate.js';

export class FormEditCreate {
	constructor(itemId) {
		this.itemId = itemId;
		this.imageSelectOptions = [];
	}

	init() {
		this.form = document.querySelector('#form');
		this.selection = this.form.querySelector('#selection');
		this.title = this.form.querySelector('#title');
		this.price = this.form.querySelector('#price');
		this.discount = this.form.querySelector('#discount');
		this.checkbox = this.form.querySelector('#checkbox');
		this.saveButton = this.form.querySelector('#save');
		this.deleteButton = this.form.querySelector('#delete');
		this.content = this.form.querySelector('#content');
		this.checkbox.onclick = this.addDiscount.bind(this);

		this.form.insertAdjacentHTML('afterbegin', Templates.getFormHeader(this.itemId ? 'Edit' : 'Add'));

		this.getPictureOptions().then(() => {
			this.saveButton.onclick = this.submitItem.bind(this);
			this.deleteButton.onclick = this.deleteItem.bind(this);
			this.getItem();
		});

		this.renderPictureOptions(this.imageSelectOptions);

		if (!this.itemId) {
			this.deleteButton.classList.toggle('hidden');
		}
	}

	addDiscount() {
		this.discount.classList.toggle('visible');
		this.discount.value = '';
	}

	getPictureOptions() {
		return RestService.get('images').then((data) => {
			this.renderPictureOptions(data);
			this.imageSelectOptions = data;
		});
	}

	renderPictureOptions(options) {
		options.forEach(option => {
			this.selection.insertAdjacentHTML('beforeend', Templates.getOptionItem(option));
		});
	}

	getItem() {
		RestService.get('items', this.itemId).then(this.itemId ? this.fillFormData.bind(this) : null);
	}

	submitItem() {
		const data = this.getFormData();
		RestService.submit('items', this.itemId, data);
	}

	deleteItem() {
		RestService.delete('items', this.itemId);
	}

	getFormData() {
		return {
			name: this.title.value,
			price: +this.price.value,
			image: this.selection.value,
			discount: +this.discount.value
		};
	}

	fillFormData(item) {
		const selectedOption = this.imageSelectOptions.find(option => option.url === item.image);

		this.title.value = item.name;
		this.price.value = item.price;
		this.selection.value = selectedOption.url;

		if (item.discount) {
			this.checkbox.checked = true;
			this.discount.classList.toggle('visible');
			this.discount.value = item.discount;
		}
	}

	renderForm() {
		return form();
	}
}


