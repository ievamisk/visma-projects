export const form = () => {
	return `<form class="form" id="form">
                <div class="form-input">
                    <label class="label" for="title">Title</label>
                    <input class="input" type="text" id="title"></input>
                </div>
                
                <div class="form-input">
                    <label class="label" for="price">Price</label>
                    <input class="input" type="text" id="price"></input>
                </div>

                <div class="form-input">
                    <label class="label" for="selection">Select item</label>
                    <select class="form-selection input" id="selection">
                        <option value="" disabled selected>Select t-shirt</option>
                    </select>
                </div>

                <div class="form-input discount">
                    <div class="checbox-block">
                        <input type="checkbox" id="checkbox">
                        <label class="label label-discount" for="selection">discount</label>
                    </div>
                    <input class="input input-price" id="discount" type="text">
                </div>

                <button type="button" class="button button-save" id="save">Save</button>
                <button type="button" class="button button-delete" id="delete">Delete</button>
            </form>`;
};