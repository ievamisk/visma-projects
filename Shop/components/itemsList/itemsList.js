import { RestService } from '../../services/restService.js';
import { Item } from '../item/item.js';
import { Loader } from '../loader/loader.js';

export class ItemsList {
	constructor() {
		this.list = document.querySelector('.card-list');
	}

	init() {
		this.getItems();
	}

	getItems() {
		const itemsTimeout = 400;

		Loader.start();

		setTimeout(() => {
			RestService.get('items').then(this.renderItems.bind(this));
		}, itemsTimeout);
	}

	renderItems(items = []) {
		this.list.innerHTML = '';

		items.forEach((item, index) => {
			this.createItem(item, index);
		});
	}

	createItem(itemObj, index) {
		const item = new Item(itemObj);

		this.list.insertAdjacentHTML('beforeend', item.renderItem(itemObj));
		this.list.querySelectorAll('.card-list-item-edit')[index].onclick = item.editItem;
	}
}