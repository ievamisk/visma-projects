// 6) Create a function which takes an array and returns an array with all duplicates removed.

const items = [
    'Arryn',
    'Targaryen',
    'Stark',
    'Bolton',
    'Greyjoy',
    'Tully',
    'Arryn',
    'Lannister',
    'Tyrell',
    'Baratheon',
    'Martell',
    'Bolton',
    'Tyrell',
    'Arryn',
];

function removeDuplicates(items){
    return uniqueArray = items.filter((item, index) =>  index === items.indexOf(item));
}

function removeDuplicatesES6(items) {
    return Array.from(new Set(items));
}

console.log('es5: ' + removeDuplicates(items), '| es6: ' + removeDuplicatesES6(items));



