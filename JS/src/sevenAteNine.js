// 9) Write a function sevenAte9 that removes each 9 that it is in between 7s.
// sevenAte9('79712312') // returns '7712312'
// sevenAte9('79797') // returns '777'

function sevenAte9(numbers) {
    return numbers.replace(/[797]+/g, '77');
}

console.log(sevenAte9('79756821797'));