// 12) Write the definition of the function "say" in such way that calling this:
// say("Hello,")("it’s me"); //Would return "Hello, it’s me";

function say(greeting) {
    return function message(message) {
        return greeting + ' ' + message;
    }
}

let msg = say('hello');
console.log(msg('hey'));

console.log(say('Hello,')('it\'s me'))
