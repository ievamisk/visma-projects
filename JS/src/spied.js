// 13) Create a simple function wrapper that will log every call to the wrapped function.
// Example : var spied = spy(myFunction);
// spied(1);
// var report = spied.report(); // returns { totalCalls: 1 }

var counter = 0;

function myFunction(num) {
    console.log('Passed number: ' + num);
}

var spy = function(func) {

    function call(num) {
        counter++;
        func(num);
    }

    function report() {
        return { totalCalls: counter };
    }

    call.report = report;
    return call;
}

var spied = spy(myFunction);

spied(1);
spied(2);
console.log(spied.report());

