// 7) Create a function wordSearch(w) that searches to see whether a word w is present in
// the given text variable. Please note it has to be a full word.

const text = 'Maecenas at orci egestas, porttitor lorem at, cursus nisi. Suspendisse porta hendrerit leo, a facilisis tortor sagittis eget. Maecenas sem nisi, tincidunt et bibendum non, pretium quis leo. Pellentesque semper lorem orci, sit amet dictum turpis ullamcorper sit amet. Donec tincidunt tincidunt ex. Ut condimentum vel ligula id posuere. Sed tristique turpis ut sagittis molestie.';

function wordSearch(wordToFind) {
    var words = text.replace('/[,\.]/', '').split(' ');
    var found = words.find((word, index) => word === wordToFind);
    console.log(words);

    if (found !== undefined) {
        return 'Word "' + wordToFind + '" is in the sentence!';
    } else {
        return 'Sorry, word "' + wordToFind + '" was not found!';
    }
}

console.log(wordSearch('eget'));