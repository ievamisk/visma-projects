// 11) Create two versions of a calculator module:
// a) Do not use any ES6 functionality
// b) Use a ES6 class
// Calculator should have four methods: add, subtract, multiply and divide. All of calculator methods should be chainable.
// Example: var calc = new Calculator(0);
// amount = calc.add(5).multiply(2).add(20).divide(3); //should return 10


function Calculator() {
    this.result = 0;
    this.add = add;
    this.substract = substract;
    this.multiply = multiply;
    this.divide = divide;

    function add(value) {
        this.result += value;
        return this;
    }

    function substract(value) {
        this.result -= value;
        return this;
    }

    function multiply(value) {
        this.result *= value;
        return this;
    }

    function divide(value) {
        this.result /= value;
        return this;
    };
}

function CalculatorPrototype() {
    this.result = 0;
}

CalculatorPrototype.prototype.add = function(value) {
    this.result += value;
    return this;
};

CalculatorPrototype.prototype.substract = function(value) {
    this.result -= value;
    return this;
};

CalculatorPrototype.prototype.multiply = function(value) {
    this.result *= value;
    return this;
};

CalculatorPrototype.prototype.divide = function(value) {
    this.result /= value;
    return this;
};

CalculatorPrototype.prototype.calculate = function() {
    console.log('With prototype: ' + this.result);
    return this;
};

class CalculatorES6 {
    constructor() {
        this.result = 0;
    }

    add(value) {
        this.result += value;
        return this;
    }

    substract(value) {
        this.result -= value;
        return this;
    }

    multiply(value) {
        this.result *= value;
        return this;
    }

    divide(value) {
        this.result /= value;
        return this;
    }
}


var calculator = new Calculator();
var answer = (calculator.add(5).multiply(2).add(20).divide(3)).result;
console.log('es5: ' + answer);

var calculatorES6 = new CalculatorES6();
var answerES6 = (calculatorES6.add(5).multiply(2).add(20).divide(3)).result;
console.log('es6: ' + answerES6);

var calculatorPrototype = new CalculatorPrototype();
calculatorPrototype.add(5).multiply(2).add(20).divide(3).calculate();