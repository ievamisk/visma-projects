// 10) Given an array with nested arrays of numbers (ex.: [10, 6, [4, 8], 3, [6, 5, [9]]]) create a
// function that would sum all numbers from provided array.

const nestedArray = [10, 6, [4, 8], 3, [6, 5, [9]]];

function sumValues(array) {
    let sum = 0;

    array.forEach((value) => {
        if (Array.isArray(value)) {
            sum += sumValues(value);
            return;
        }
        sum += value;
    });

    return sum;
}

console.log(sumValues(nestedArray));