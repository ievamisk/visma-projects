// 4) Create two versions of a function called sum which takes a list of numbers and returns a
// sum of them.
// a) Do not use any ES6 methods
// b) Use a suitable ES6 method

const numbersList = [1, 5, 10, 9, 2, 6];

function sum(numbers) {
    let sum = 0;

    numbers.forEach(number => {
        sum += number;
    });

    return sum;
}

function sumES6(numbers) {
    return numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
}

console.log('es5: ' + sum(numbersList), '| es6: ' + sumES6(numbersList));

