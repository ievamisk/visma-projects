// 3) Create two versions of a function called range which takes two numbers x and y and
// returns an array filled with all numbers from x (inclusive) to y (exclusive)
// a) Do not use any ES6 methods
// b) Use a suitable ES6 method

function range(x, y) {
    var numbers = []
    for (let i = x; i < y; i ++) {
        numbers.push(i);
    }
    return numbers;
};

function rangeES6(x, y) {
    return Array(y - x)
        .fill()
        .map((item, index) => index + x)
};

console.log('es5: ' + range(1, 10), '| es6: ' + rangeES6(1, 10));


