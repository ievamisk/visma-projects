// 1) Write a loop that makes seven calls to console.log to output the following triangle:
// #
// ##
// ###
// ####
// #####
// ######
// #######
// Print Every line in random color every time (choose from colors red, green, yellow, blue).
// There should not be two lines of the same color next to each other.

var colors = require('colors/safe');

const colorsArray = [
    colors.red, 
    colors.green, 
    colors.yellow, 
    colors.blue
];

function getRandomColor(colors, previousColor = null) {
    color = colors[Math.floor(Math.random() * colors.length)];

    while (color === previousColor) {
        color = colors[Math.floor(Math.random() * colors.length)];
    }

    return color;
}

function printTriangle(size) {
    let previousColor = null; 

    if (size !== null) {
        for (let i = 0; i < size; i++) {
            let line = '';
            for (let j = 0; j < i + 1; j++) {
                line += '#';
            }
            previousColor = getRandomColor(colorsArray, previousColor);
            console.log(color(line));
        }
    }
}

printTriangle(7);
getRandomColor(colorsArray);


